import sys
import os
import subprocess
import shlex

REPO = None

git_cmd_output = ""


def git_set_repo_path(repo_path):
    global REPO
    if os.path.exists(repo_path):
        REPO = repo_path
        return True
    return False


def git_repo_path():
    global REPO
    return REPO


def git_last_output():
    global git_cmd_output
    return git_cmd_output


def git_set_last_output(last_output):
    global git_cmd_output
    git_cmd_output = str(last_output)


def fail_with_msg(msg, err_code = 1):
    print "FATAL:", msg
    sys.exit(err_code)


def run_cmd(cmd_line):
    if not REPO is None:
        if not os.path.exists(REPO):
            raise Exception("Local repository " + REPO + " does not exist!")
    cmd_list = shlex.split(cmd_line)
    out = ""
    ret = True
    try:
        out = subprocess.check_output(cmd_list, cwd=REPO, shell=True, stderr=subprocess.STDOUT)
    except Exception as e:
        out = str(e)
        ret = False
    print " ".join(cmd_list)
    if len(out) > 0:
        print out
    print "result =", ret
    return ret, out


def git_cmd(sub_cmd, args_str):
    global git_cmd_output
    cmd_line = "git " + sub_cmd + " " + args_str
    ret, out = run_cmd(cmd_line)
    git_cmd_output = out
    return ret


def git_create_branch(branch_name, starting_point):
    return git_cmd("checkout", "-b " + branch_name + " " + starting_point)


def git_switch_branch(branch_name):
    return git_cmd("checkout", branch_name)


def git_branch_exists(branch_name):
    print "git_branch_exists(" + branch_name + ")"
    return git_cmd("rev-parse", "--verify " + branch_name)


def git_delete_local_branch(branch_name):
    print "git_delete_local_branch(" + branch_name + ")"
    if not git_branch_exists(branch_name):
        return True
    return git_cmd("branch", "-D " + branch_name)


def git_track_branch(branch_name):
    print "git_track_remote_branch(" + branch_name + ")"
    if not git_branch_exists("origin/" + branch_name):
        return False
    if git_branch_exists(branch_name):
        return True
    return git_cmd("checkout", "-b " + branch_name + " origin/" + branch_name)


def git_pull_with_rebase(branch_name):
    print "git_pull_with_rebase(" + branch_name + ")"
    if not git_switch_branch(branch_name):
        return False
    return git_cmd("pull", "--rebase")


def git_rebase(exclude_point, branch_to_rebase, onto_point=None):
    print "git_rebase(" + exclude_point + ", " + branch_to_rebase + ", " + str(onto_point) + ")"
    if onto_point is None:
        onto_point = exclude_point
    args_str = "--onto " + onto_point + " " + exclude_point + " "  + branch_to_rebase
    return git_cmd("rebase", args_str)


def git_merge_base(branch_a, branch_b):
    global git_cmd_output
    print "git_merge_base(" + branch_a + ", " + branch_b + ")"
    if git_cmd("merge-base", branch_a + " " + branch_b):
        print "merge base is", git_cmd_output
        return git_cmd_output
    return None


def git_ff_merge_into(into_branch, branch_to_merge):
    print "git_ff_merge_into(" + into_branch + ", " + branch_to_merge + ")"
    if not git_switch_branch(into_branch):
        return False
    return git_cmd("merge", "--ff-only " + branch_to_merge)

def git_push_branch(branch):
    global git_cmd_output
    print "git_push_branch(" + branch + ")"
    if not git_branch_exists("origin/" + branch_name):
        git_cmd_output = "Remote branch " + branch_name + " does not exist!"
        return False
    return git_cmd("push", "origin " + branch_name)


def git_push_new_branch(branch):
    global git_cmd_output
    print "git_push_new_branch(" + branch + ")"
    if git_branch_exists("origin/" + branch):
        git_cmd_output = "Remote branch " + branch + " already exists!"
        return False
    return git_cmd("push", "-u origin " + branch)


def git_integration_branch():
    print "git_integration_branch()"
    integration_branch = None
    if not git_branch_exists("integration"):
        if git_branch_exists("Nimbus/integration"):
            integration_branch = "Nimbus/integration"
    else:
        integration_branch = "integration"
    return integration_branch


def git_testing_branch():
    print "git_testing_branch()"
    testing_branch = None
    if not git_branch_exists("testing"):
        if git_branch_exists("Nimbus/testing"):
            integration_branch = "Nimbus/testing"
    else:
        integration_branch = "testing"
    return testing_branch


def git_update_integration():
    global git_cmd_output
    print "git_update_integration()"
    integration_branch = git_integration_branch()
    if integration_branch is None:
        git_cmd_output = "No integration branch exists in the repo!"
        return False
    return git_pull_with_rebase(integration_branch)


def git_update_testing():
    global git_cmd_output
    print "git_update_testing()"
    testing_branch = git_testing_branch()
    if testing_branch is None:
        git_cmd_output = "No testing branch exists in the repo!"
        return False
    return git_pull_with_rebase(testing_branch)


def git_push_integration():
    global git_cmd_output
    print "git_push_integration()"
    integration_branch = git_integration_branch()
    if integration_branch is None:
        git_cmd_output = "No integration branch exists in the repo!"
        return False
    return git_cmd("push", "origin " + integration_branch)


def git_project_prefix():
    project_prefix = ""
    if git_branch_exists("Nimbus/master"):
        project_prefix = "Nimbus/"
    return project_prefix
