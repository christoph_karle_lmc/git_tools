from git_tools import *
from git_transplant import *


if len(sys.argv) < 3:
    print "git_rvw2acc <demo branch (w/o CI/ prefix)> <review branch (w/o rvw/ prefix)> [more review branches ...]"
    sys.exit(1)

PROJECT_PREFIX = git_project_prefix()
DEMO_BRANCH = PROJECT_PREFIX + "CI/" + sys.argv[1]
BRANCHES = sys.argv[2:]

SRC_PREFIX = PROJECT_PREFIX + "rvw-3.0"
DEST_PREFIX = PROJECT_PREFIX + "acc-3.0"

# The demo branch is allowed to already exist in origin
if not git_branch_exists("origin/" + DEMO_BRANCH):
    if not git_new_branch_from_integration(DEMO_BRANCH):
        fail_with_msg("Creating " + DEMO_BRANCH + " failed : " + git_last_output())
else:
    if not git_branch_exists(DEMO_BRANCH):
        fail_with_msg("Someone else already created " + DEMO_BRANCH + " on origin")


if not git_track_all_branches(SRC_PREFIX, *BRANCHES):
    fail_with_msg("Could not track all " + SRC_PREFIX +  ", ".join(BRANCHES) + " branches : " + git_last_output())

if not git_no_branches_exist("origin/" + DEST_PREFIX, *BRANCHES):
    fail_with_msg("Some remote " + DEST_PREFIX +  ", ".join(BRANCHES) + " branches already exist : " + git_last_output())

if not git_transplant(DEMO_BRANCH, SRC_PREFIX, DEST_PREFIX, *BRANCHES):
    fail_with_msg("Could not transplant " + ", ".join(BRANCHES) + " branches onto " + DEMO_BRANCH + " : " + git_last_output())

#if not git_push_new_branches(DEST_PREFIX, *BRANCHES):
#    fail_with_msg("Could not push new " + DEST_PREFIX + ", ".join(BRANCHES) + " branches : " + git_last_output())

#if not git_branch_exists("origin/" + DEMO_BRANCH):
#    if not git_push_new_branch(DEMO_BRANCH):
#        fail_with_msg("Could not push new " + DEMO_BRANCH + " branch : " + git_last_output())
#else:
#    if not git_push_branch(DEMO_BRANCH):
#        fail_with_msg("Could not push " + DEMO_BRANCH + " branch : " + git_last_output())
