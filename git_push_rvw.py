from git_tools import *
from git_transplant import *


if len(sys.argv) < 3:
    print "git_push_rvw <CI branch (w/o CI/ prefix)> <review branch (w/o rvw/ prefix)> [more review branches ...]"
    sys.exit(1)

PROJECT_PREFIX = git_project_prefix()
CI_BRANCH = PROJECT_PREFIX + "CI/" + sys.argv[1]
BRANCHES = sys.argv[2:]

RVW_PREFIX = PROJECT_PREFIX + "rvw-3.0"

if not git_push_new_branches(RVW_PREFIX, *BRANCHES):
    fail_with_msg("Could not push new " + RVW_PREFIX + ", ".join(BRANCHES) + " branches : " + git_last_output())

if not git_push_new_branch(CI_BRANCH):
    fail_with_msg("Could not push new " + CI_BRANCH + " branch : " + git_last_output())
