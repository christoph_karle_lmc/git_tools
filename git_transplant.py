from git_tools import *


def git_track_all_branches(prefix, *branches):
    global git_cmd_output
    if prefix[-1] != "/":
        prefix += "/"
    for branch in branches:
        if not git_track_branch(prefix + branch):
            git_set_last_output("Remote branch " + prefix + branch + " does not exist!")
            return False
    return True


def git_all_branches_exist(prefix, *branches):
    global git_cmd_output
    if prefix[-1] != "/":
        prefix += "/"
    for branch in branches:
        if not git_branch_exists(prefix + branch):
            git_set_last_output("Branch " + prefix + branch + " does not exist!")
            return False
    return True


def git_no_branches_exist(prefix, *branches):
    global git_cmd_output
    if prefix[-1] != "/":
        prefix += "/"
    for branch in branches:
        if git_branch_exists(prefix + branch):
            git_set_last_output("Branch " + prefix + branch + " already exists!")
            return False
    return True


def git_new_branch_from_integration(branch):
    global git_cmd_output
    if git_branch_exists("origin/" + branch):
        git_set_last_output("Remote branch "  + branch + " already exists!")
        return False
    if not git_branch_exists(branch):
        if not git_update_integration():
            git_set_last_output("Could not update mainline integration branch : " + git_last_output())
            return False
        integration_branch = git_integration_branch()
        if None == integration_branch:
            git_set_last_output("Could not find a mainline integration branch : " + git_last_output())
            return False
        if not git_create_branch(branch, integration_branch):
            git_set_last_output("Could not create " + branch + " branch!")
            return False
    return True


def git_push_branches(prefix, *branches):
    if prefix[-1] != "/":
        prefix += "/"
    for branch in branches:
        if not git_push_branch(prefix + branch):
            git_set_last_output("Could not push " + prefix + branch + " branch : " + git_last_output())
            return False
    return True


def git_push_new_branches(prefix, *branches):
    if prefix[-1] != "/":
        prefix += "/"
    for branch in branches:
        if not git_push_new_branch(prefix + branch):
            git_set_last_output("Could not push new " + prefix + branch + " branch : " + git_last_output())
            return False
    return True


def git_transplant(onto_branch, src_prefix, dest_prefix, *branches):
    global git_cmd_output
    if src_prefix[-1] != "/":
        src_prefix += "/"
    if dest_prefix[-1] != "/":
        dest_prefix += "/"

    if not git_branch_exists(onto_branch):
        git_set_last_output("Onto branch " + onto_branch + " does not exist!")
        return False

    if not git_all_branches_exist(src_prefix, *branches):
        git_set_last_output("Not all source branches exist : " + git_last_output())
        return False

    # "Copy" src into dest branches and rebase them on top of onto_branch
    last_branch = onto_branch
    for branch in branches:
        src_branch = src_prefix + branch
        dest_branch = dest_prefix + branch
        if not git_branch_exists(dest_branch):
            # We assume that if the branch already exists, it is already rebased
            if not git_create_branch(dest_branch, src_branch):
                git_set_last_output("Could not create " + dest_branch + "!")
                return False
            # Get the last common commit between last_branch and dest_branch
            branch_point = git_merge_base(last_branch, dest_branch)
            if None == branch_point:
                git_set_last_output("Could not find a merge base for " + last_branch + " and " + dest_branch + "!")
                return False
            # Use branch_point as the exclusion point to avoid getting commits back in
            # that were changed due to rebase conflicts in previously rebased branches
            if not git_rebase(branch_point, dest_branch, onto_branch):
                git_set_last_output("Could not rebase " + dest_branch + " onto " + onto_branch + "!")
                return False
        # The fast-forward merge needs to be done whether the branch already existed or not
        # If the branch already existed, it just makes sure that the onto_branch points to the correct commit
        # for the next branches to be rebased on top of it
        if not git_ff_merge_into(onto_branch, dest_branch):
            git_set_last_output("Fast-forward merge" + dest_branch + " into " + onto_branch + " failed!")
            return False
        last_branch = src_branch

    git_set_last_output("")
    return True
