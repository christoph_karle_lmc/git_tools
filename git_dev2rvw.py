from git_tools import *
from git_transplant import *

if len(sys.argv) < 3:
    print "git_dev2rvw <CI branch (w/o CI/ prefix)> <dev branch (w/o dev/ prefix)> [more dev/ branches ...]"
    sys.exit(1)

PROJECT_PREFIX = git_project_prefix()
CI_BRANCH = PROJECT_PREFIX + "CI/" + sys.argv[1]
BRANCHES = sys.argv[2:]

SRC_PREFIX = PROJECT_PREFIX + "dev-3.0"
DEST_PREFIX = PROJECT_PREFIX + "rvw-3.0"

if git_branch_exists("origin/" + CI_BRANCH):
    fail_with_msg("Remote branch "  + CI_BRANCH + " already exists!")

if not git_new_branch_from_integration(CI_BRANCH):
    fail_with_msg("Creating " + CI_BRANCH + " failed : " + git_last_output())

if not git_track_all_branches(SRC_PREFIX, *BRANCHES):
    fail_with_msg("Could not track all " + SRC_PREFIX + " " +  ", ".join(BRANCHES) + " branches : " + git_last_output())

if not git_no_branches_exist("origin/" + DEST_PREFIX, *BRANCHES):
    fail_with_msg("Some remote " + DEST_PREFIX +  ", ".join(BRANCHES) + " branches already exist : " + git_last_output())

if not git_transplant(CI_BRANCH, SRC_PREFIX, DEST_PREFIX, *BRANCHES):
    fail_with_msg("Could not transplant " + ", ".join(BRANCHES) + " branches onto " + CI_BRANCH + " : " + git_last_output())
